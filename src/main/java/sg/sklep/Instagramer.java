package sg.sklep;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Instagramer extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        out.println("<html>");
        out.println("<body>\n" +
                "<h2>Panel programow partnerskich</h2>\n" +
                "<form action=\"instagramer\" method=\"POST\">" +
                "Id Produktu: <input name=\"idProduktu\" type=\"text\"/>" +
                "<br/>" +
                "Login: <input name=\"login\" type=\"text\"/>" +
                "<br/>" +
                "Prowizja: <input name=\"prowizja\" type=\"text\"/>" +
                "<br/>" +
                "E-mail: <input name=\"email\" type=\"text\"/> " +
                "<br/>" +
                "Kod znizkowy: <input name=\"kodZnizkowy\" type=\"text\"/>" +
                "<br/>" +
                " <input value=\"Zatwierdz\" type=\"submit\"/>" +
                "</form>" +
                "</body>\n");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int idProd = Integer.parseInt(req.getParameter("idProduktu"));
        String login = req.getParameter("login");
        int prowizja = Integer.parseInt(req.getParameter("prowizja"));
        String email = req.getParameter("email");
        String kodZnizkowy = req.getParameter("kodZnizkowy");
        resp.getWriter().println("Dane: " + " \n" +
                "Id produktu: " + idProd + " \n" +
                "Login: " + login + "\n"  +
                "prowizja: " + prowizja + "\n"  +
                "e-mail: " + email  + "\n"  +
                "kod znizkowy: " + kodZnizkowy + " \n" );
    }
}
