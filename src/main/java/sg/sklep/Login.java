package sg.sklep;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Login extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        out.println("<html>");
        out.println("<body>\n" +
                "<h2>Panel logowania</h2>\n" +
                "<form action=\"login\" method=\"POST\">" +
                    "Login: <input name=\"login\" type=\"text\"/> " +
                        "<br/>"+
                    "Haslo: <input name=\"password\" type=\"password\"/>" +
                    "<br/>" +
                    "<input value=\"Zaloguj\" type=\"submit\"/>" +
                "</form>" +
                "</body>\n");
        out.println("</html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        resp.getWriter().println("Logowanie korzystajac z danych: " + login + "/" + password);
    }
}
